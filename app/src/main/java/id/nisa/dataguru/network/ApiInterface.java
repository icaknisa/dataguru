package id.nisa.dataguru.network;

import java.util.ArrayList;

import id.nisa.dataguru.model.add.ResponseAdd;
import id.nisa.dataguru.model.delete.ResponseDelete;
import id.nisa.dataguru.model.login.ResponseLogin;
import id.nisa.dataguru.model.readGuru.ResponseReadGuru;
import id.nisa.dataguru.model.register.ResponseRegister;

import id.nisa.dataguru.model.update.ResponseUpdate;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiInterface {

    //To Do Login
    @FormUrlEncoded
    @POST("login_user.php")
    Call<ResponseLogin> responseLogin(@Field("vsusername") String vsusername,
                                      @Field("vspassword") String vspassword);

    //To Do Register
    @FormUrlEncoded
    @POST("register_user.php")
    Call<ResponseRegister> responseRegister(@Field("nama_user") String nama_user,
                                            @Field("vsusername") String vsusername,
                                            @Field("vspassword") String vspassword);

    @GET("read_guru.php")
    Call<ArrayList<ResponseReadGuru>> actionReadGuru();

    @PUT("update_guru.php")
    Call<ResponseUpdate> actionUpdate(@Query("id_guru") String id_guru,
                                      @Query("nama_guru") String nama_guru,
                                      @Query("kelas") String kelas);

    @DELETE("delete_guru.php")
    Call<ResponseDelete> actionDelete(@Query("id_guru") String id);

    @FormUrlEncoded
    @POST("create_guru.php")
    Call<ResponseAdd> actionAdd(@Field("nama_guru") String nama_guru,
                                @Field("kelas") String kelas);


}