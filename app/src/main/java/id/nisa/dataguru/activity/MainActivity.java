package id.nisa.dataguru.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nisa.dataguru.R;
import id.nisa.dataguru.RecyclerViewAdapter;
import id.nisa.dataguru.model.readGuru.ResponseReadGuru;
import id.nisa.dataguru.network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ArrayList<ResponseReadGuru> data = null;

    @BindView(R.id.rcView)
    RecyclerView rcView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getReadGuru();
    }

    private void getReadGuru() {
        ApiClient.service.actionReadGuru().enqueue(new Callback<ArrayList<ResponseReadGuru>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseReadGuru>> call, Response<ArrayList<ResponseReadGuru>> response) {
                if (response.code() == 200) {
                    data = response.body();
                    if (data == null) {
                        Toast.makeText(MainActivity.this, "Data NULL", Toast.LENGTH_SHORT).show();
                    } else {
                        rcView.setHasFixedSize(true);
                        rcView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        rcView.setAdapter(new RecyclerViewAdapter(MainActivity.this, data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseReadGuru>> call, Throwable t) {

            }
        });


    }

    // ketik OnCreateOptionMenu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // ketik OnOptionItem
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_tambah:
                startActivity(new Intent(MainActivity.this, AddActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
