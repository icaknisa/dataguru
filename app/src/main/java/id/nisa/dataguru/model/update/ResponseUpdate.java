package id.nisa.dataguru.model.update;

import com.google.gson.annotations.SerializedName;

public class ResponseUpdate{

	@SerializedName("kelas")
	private String kelas;

	@SerializedName("nama_guru")
	private String namaGuru;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setKelas(String kelas){
		this.kelas = kelas;
	}

	public String getKelas(){
		return kelas;
	}

	public void setNamaGuru(String namaGuru){
		this.namaGuru = namaGuru;
	}

	public String getNamaGuru(){
		return namaGuru;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseUpdate{" + 
			"kelas = '" + kelas + '\'' + 
			",nama_guru = '" + namaGuru + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}