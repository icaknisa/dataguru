package id.nisa.dataguru.model.readGuru;

import com.google.gson.annotations.SerializedName;

public class ResponseReadGuru{

	@SerializedName("kelas")
	private String kelas;

	@SerializedName("id_guru")
	private String idGuru;

	@SerializedName("nama_guru")
	private String namaGuru;

	public void setKelas(String kelas){
		this.kelas = kelas;
	}

	public String getKelas(){
		return kelas;
	}

	public void setIdGuru(String idGuru){
		this.idGuru = idGuru;
	}

	public String getIdGuru(){
		return idGuru;
	}

	public void setNamaGuru(String namaGuru){
		this.namaGuru = namaGuru;
	}

	public String getNamaGuru(){
		return namaGuru;
	}

	@Override
 	public String toString(){
		return 
			"ResponseReadGuru{" + 
			"kelas = '" + kelas + '\'' + 
			",id_guru = '" + idGuru + '\'' + 
			",nama_guru = '" + namaGuru + '\'' + 
			"}";
		}
}