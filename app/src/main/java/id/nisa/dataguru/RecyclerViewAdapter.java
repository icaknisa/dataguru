package id.nisa.dataguru;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.nisa.dataguru.model.readGuru.ResponseReadGuru;
import id.nisa.dataguru.activity.DetailActivity;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {


    List<ResponseReadGuru> dataReadGuru;
    Context context;

    public RecyclerViewAdapter(Context context, ArrayList<ResponseReadGuru> dataReadGuru){
        this.context = context;
        this.dataReadGuru = dataReadGuru;
    }
    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {

        String namaGuru = dataReadGuru.get(position).getNamaGuru();
        String kelasGuru = dataReadGuru.get(position).getKelas();
        String idGuru = dataReadGuru.get(position).getIdGuru();

        holder.txtNamaGuru.setText(namaGuru);
        holder.txtKelasGuru.setText(kelasGuru);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra(DetailActivity.KEY_NAMA_GURU, namaGuru);
                intent.putExtra(DetailActivity.KEY_KELAS_GURU,kelasGuru);
                intent.putExtra(DetailActivity.KEY_ID,idGuru);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataReadGuru.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNamaGuru, txtKelasGuru;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNamaGuru = itemView.findViewById(R.id.txtListNamaGuru);
            txtKelasGuru = itemView.findViewById(R.id.txtListKelasGuru);
        }
    }
}
